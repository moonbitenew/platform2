<?php

return [
    'title' => [
        'slider' => 'Slider',
    ],
    'popup' => [
        'slide_content' => 'Opis slijdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
];
