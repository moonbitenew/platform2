<?php

return [
    'only one primary menu' => 'Only one menu can be primary at a time',
    'name is required' => 'Nazwa jest wymagana',
];
