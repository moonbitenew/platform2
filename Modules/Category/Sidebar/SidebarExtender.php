<?php

namespace Modules\Category\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender {
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param category $category
     *
     * @return category
     */
    public function extendWith(Menu $category)
    {
        $category->group(trans('core::sidebar.content'), function (Group $group) {
            $group->weight(90);
            $group->item(trans('category::category.title'), function (Item $item) {
                $item->weight(3);
                $item->icon('fa fa-share-alt');
                $item->route('admin.category.category.index');
                $item->authorize(
                    $this->auth->hasAccess('category.categories.index')
                );
            });
        });

        return $category;
    }
}
