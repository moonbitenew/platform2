<?php

namespace Modules\Category\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Category\Blade\CategoryDirective;
use Modules\Category\Entities\Category;
use Modules\Category\Entities\Categoryitem;
use Modules\Category\Repositories\Cache\CacheCategoryDecorator;
use Modules\Category\Repositories\Cache\CacheCategoryItemDecorator;
use Modules\Category\Repositories\Eloquent\EloquentCategoryItemRepository;
use Modules\Category\Repositories\Eloquent\EloquentCategoryRepository;
use Nwidart\Menus\CategoryBuilder as Builder;
use Nwidart\Menus\Facades\Menu as CategoryFacade;
use Nwidart\Menus\MenuItem as PingpongCategoryItem;

class CategoryServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();

        $this->app->bind('category.category.directive', function () {
            return new CategoryDirective();
        });
    }

    /**
     * Register all online categorys on the Pingpong/Category package
     */
    public function boot()
    {
        $this->registerCategories();
        $this->registerBladeTags();
        $this->publishConfig('category', 'permissions');
        $this->publishConfig('category', 'config');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    /**
     * Register class binding
     */
    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Category\Repositories\CategoryRepository',
            function () {
                $repository = new EloquentCategoryRepository(new Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new CacheCategoryDecorator($repository);
            }
        );

        $this->app->bind(
            'Modules\Category\Repositories\CategoryItemRepository',
            function () {
                $repository = new EloquentCategoryItemRepository(new Categoryitem());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new CacheCategoryItemDecorator($repository);
            }
        );
    }

    /**
     * Add a category item to the category
     * @param Categoryitem $item
     * @param Builder $category
     */
    public function addItemToCategory(Categoryitem $item, Builder $category)
    {
        if ($this->hasChildren($item)) {
            $this->addChildrenToCategory($item->title, $item->items, $category, ['icon' => $item->icon, 'target' => $item->target]);
        } else {
            $target = $item->link_type != 'external' ? $item->locale . '/' . $item->uri : $item->url;
            $category->url(
                $target,
                $item->title,
                [
                    'target' => $item->target,
                    'icon' => $item->icon,
                    'class' => $item->class,
                ]
            );
        }
    }

    /**
     * Add children to category under the give name
     *
     * @param string $name
     * @param object $children
     * @param Builder|CategoryItem $category
     */
    private function addChildrenToCategory($name, $children, $category, $attribs = [])
    {
        $category->dropdown($name, function (PingpongCategoryItem $subCategory) use ($children) {
            foreach ($children as $child) {
                $this->addSubItemToCategory($child, $subCategory);
            }
        }, 0, $attribs);
    }

    /**
     * Add children to the given category recursively
     * @param Categoryitem $child
     * @param PingpongCategoryItem $sub
     */
    private function addSubItemToCategory(Categoryitem $child, PingpongCategoryItem $sub)
    {
        if ($this->hasChildren($child)) {
            $this->addChildrenToCategory($child->title, $child->items, $sub);
        } else {
            $target = $child->link_type != 'external' ? $child->locale . '/' . $child->uri : $child->url;
            $sub->url($target, $child->title, 0, ['icon' => $child->icon, 'target' => $child->target, 'class' => $child->class]);
        }
    }

    /**
     * Check if the given category item has children
     *
     * @param  object $item
     * @return bool
     */
    private function hasChildren($item)
    {
        return $item->items->count() > 0;
    }

    /**
     * Register the active categorys
     */
    private function registerCategories()
    {
        if (! $this->app['asgard.isInstalled']) {
            return;
        }
        $category = $this->app->make('Modules\Category\Repositories\CategoryRepository');
        $categoryItem = $this->app->make('Modules\Category\Repositories\CategoryItemRepository');
        foreach ($category->allOnline() as $category) {
            $categoryTree = $categoryItem->getTreeForCategory($category->id);
            CategoryFacade::create($category->name, function (Builder $category) use ($categoryTree) {
                foreach ($categoryTree as $categoryItem) {
                    $this->addItemToCategory($categoryItem, $category);
                }
            });
        }
    }

    /**
     * Register category blade tags
     */
    protected function registerBladeTags()
    {
        if (app()->environment() === 'testing') {
            return;
        }

        $this->app['blade.compiler']->directive('category', function ($arguments) {
            return "<?php echo CategoryDirective::show([$arguments]); ?>";
        });
    }
}
