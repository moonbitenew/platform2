<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    public function rules()
    {
        $category = $this->route()->getParameter('category');

        return [
            'name' => 'required',
            'primary' => "unique:category__categories,primary,{$category->id}",
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => trans('category::validation.name is required'),
            'primary.unique' => trans('category::validation.only one primary category'),
        ];
    }
}
