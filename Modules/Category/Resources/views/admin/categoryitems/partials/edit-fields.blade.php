<div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
    {!! Form::label('icon', trans('category::category-items.form.icon')) !!}
    {!! Form::text('icon', old('icon', $categoryItem->icon), ['class' => 'form-control', 'placeholder' => trans('category::category-items.form.icon')]) !!}
    {!! $errors->first('icon', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
    {!! Form::label('class', trans('category::category-items.form.class')) !!}
    {!! Form::text('class', old('class',$categoryItem->class), ['class' => 'form-control']) !!}
    {!! $errors->first('class', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group link-type-depended link-page">
    <label for="page">{{ trans('category::category-items.form.page') }}</label>
    <select class="form-control" name="page_id" id="page">
        <option value=""></option>
        <?php foreach ($pages as $page): ?>
        <option value="{{ $page->id }}" {{ $categoryItem->page_id == $page->id ? 'selected' : '' }}>
            {{ $page->title }}
        </option>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label for="parent_id">{{ trans('category::category-items.form.parent category item') }}</label>
    <select class="form-control" name="parent_id" id="parent_id">
        <option value=""></option>
        <?php foreach ($categorySelect as $parentCategoryItemId => $parentCategoryItemName): ?>
        <?php if ($categoryItem->id != $parentCategoryItemId): ?>
        <option value="{{ $parentCategoryItemId }}" {{ $categoryItem->parent_id == $parentCategoryItemId ? ' selected' : '' }}>{{ $parentCategoryItemName }}</option>
        <?php endif; ?>
        <?php endforeach; ?>
    </select>
</div>
<div class="form-group">
    <label for="target">{{ trans('category::category-items.form.target') }}</label>
    <select class="form-control" name="target" id="target">
        <option value="_self" {{ $categoryItem->target === '_self' ? 'selected' : '' }}>{{ trans('category::category-items.form.same tab') }}</option>
        <option value="_blank" {{ $categoryItem->target === '_blank' ? 'selected' : '' }}>{{ trans('category::category-items.form.new tab') }}</option>
    </select>
</div>
