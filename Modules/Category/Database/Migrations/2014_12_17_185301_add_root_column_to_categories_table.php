<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRootColumnToCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category__categoryitems', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->boolean('is_root')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category__categoryitems', function (Blueprint $table) {
            $table->dropColumn('is_root');
        });
    }
}
