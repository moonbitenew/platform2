<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIconColumnToCategoryitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category__categoryitems', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('icon')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category__categoryitems', function (Blueprint $table) {
            $table->dropColumn('icon');
        });
    }
}
