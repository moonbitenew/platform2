<?php

namespace Modules\Category\Services;

use Illuminate\Support\Facades\URL;

class CategoryRenderer
{
    /**
     * @var int Id of the Category to render
     */
    protected $categoryId;
    /**
     * @var string
     */
    private $startTag = '<div class="dd">';
    /**
     * @var string
     */
    private $endTag = '</div>';
    /**
     * @var string
     */
    private $category = '';

    /**
     * @param $categoryId
     * @param $categoryItems
     * @return string
     */
    public function renderForCategory($categoryId, $categoryItems)
    {
        $this->categoryId = $categoryId;

        $this->category .= $this->startTag;
        $this->generateHtmlFor($categoryItems);
        $this->category .= $this->endTag;

        return $this->category;
    }

    /**
     * Generate the html for the given items
     * @param $items
     */
    private function generateHtmlFor($items)
    {
        $this->category .= '<ol class="dd-list">';
        foreach ($items as $item) {
            $this->category .= "<li class=\"dd-item\" data-id=\"{$item->id}\">";
            $editLink = URL::route('dashboard.categoryitem.edit', [$this->categoryId, $item->id]);
            $style = $item->isRoot() ? 'none' : 'inline';
            $this->category .= <<<HTML
<div class="btn-group" role="group" aria-label="Action buttons" style="display: {$style}">
    <a class="btn btn-sm btn-info" style="float:left;" href="{$editLink}">
        <i class="fa fa-pencil"></i>
    </a>
    <a class="btn btn-sm btn-danger jsDeleteCategoryItem" style="float:left; margin-right: 15px;" data-item-id="{$item->id}">
       <i class="fa fa-times"></i>
    </a>
</div>
HTML;
            $handleClass = $item->isRoot() ? 'dd-handle-root' : 'dd-handle';
            if (isset($item->icon) && $item->icon != '') {
                $this->category .= "<div class=\"{$handleClass}\"><i class=\"{$item->icon}\" ></i> {$item->title}</div>";
            } else {
                $this->category .= "<div class=\"{$handleClass}\">{$item->title}</div>";
            }

            if ($this->hasChildren($item)) {
                $this->generateHtmlFor($item->items);
            }

            $this->category .= '</li>';
        }
        $this->category .= '</ol>';
    }

    /**
     * @param $item
     * @return bool
     */
    private function hasChildren($item)
    {
        return count($item->items);
    }
}
