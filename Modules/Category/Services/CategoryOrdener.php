<?php

namespace Modules\Category\Services;

use Modules\Category\Entities\Categoryitem;
use Modules\Category\Repositories\CategoryItemRepository;

class CategoryOrdener
{
    /**
     * @var CategoryuItemRepository
     */
    private $categoryItemRepository;

    /**
     * @param CategoryItemRepository $categoryItem
     */
    public function __construct(CategoryItemRepository $categoryItem)
    {
        $this->categoryItemRepository = $categoryItem;
    }

    /**
     * @param $data
     */
    public function handle($data)
    {
        $data = $this->convertToArray(json_decode($data));

        foreach ($data as $position => $item) {
            $this->order($position, $item);
        }
    }

    /**
     * Order recursively the category items
     * @param int   $position
     * @param array $item
     */
    private function order($position, $item)
    {
        $categoryItem = $this->categoryItemRepository->find($item['id']);
        if (0 === $position && false === $categoryItem->isRoot()) {
            return;
        }
        $this->savePosition($categoryItem, $position);
        $this->makeItemChildOf($categoryItem, null);

        if ($this->hasChildren($item)) {
            $this->handleChildrenForParent($categoryItem, $item['children']);
        }
    }

    /**
     * @param Menuitem $parent
     * @param array    $children
     */
    private function handleChildrenForParent(Categoryitem $parent, array $children)
    {
        foreach ($children as $position => $item) {
            $categoryItem = $this->categoryItemRepository->find($item['id']);
            $this->savePosition($categoryItem, $position);
            $this->makeItemChildOf($categoryItem, $parent->id);

            if ($this->hasChildren($item)) {
                $this->handleChildrenForParent($categoryItem, $item['children']);
            }
        }
    }

    /**
     * Save the given position on the category item
     * @param object $categoryItem
     * @param int    $position
     */
    private function savePosition($categoryItem, $position)
    {
        $this->categoryItemRepository->update($categoryItem, compact('position'));
    }

    /**
     * Check if the item has children
     *
     * @param  array $item
     * @return bool
     */
    private function hasChildren($item)
    {
        return isset($item['children']);
    }

    /**
     * Set the given parent id on the given category item
     *
     * @param object $item
     * @param int    $parent_id
     */
    private function makeItemChildOf($item, $parent_id)
    {
        $this->categoryItemRepository->update($item, compact('parent_id'));
    }

    /**
     * Convert the object to array
     * @param $data
     * @return array
     */
    private function convertToArray($data)
    {
        $data = json_decode(json_encode($data), true);

        return $data;
    }
}
