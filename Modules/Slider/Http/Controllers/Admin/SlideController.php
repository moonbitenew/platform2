<?php

namespace Modules\Slider\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Slider\Entities\Slide;
use Modules\Slider\Repositories\SlideRepository;
use Modules\Slider\Http\Requests\CreateSlideRequest;
use Modules\Slider\Http\Requests\UpdateSlideRequest;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class SlideController extends AdminBaseController {

    /**
     * @var SlideRepository
     */
    private $slide;

    public function __construct(SlideRepository $slide) {
        parent::__construct();

        $this->slide = $slide;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //$slides = $this->slide->all();

        return view('slider::admin.slides.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('slider::admin.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(CreateSlideRequest $request) {
        $this->slide->create($request->all());

        return redirect()->route('admin.slider.slider.edit', $request->slider_id)
                        ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('slider::slides.title.slides')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Slide $slide
     * @return Response
     */
    public function edit(Slide $slide) {
        return view('slider::admin.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Slide $slide
     * @param  Request $request
     * @return Response
     */
    public function update(Slide $slide, UpdateSlideRequest $request) {
        $this->slide->update($slide, $request->all());

        return redirect()->route('admin.slider.slider.edit', $request->slider_id)
                        ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('slider::slides.title.slides')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Slide $slide
     * @return Response
     */
    public function destroy(Slide $slide) {
        $this->slide->destroy($slide);

        return redirect()->route('admin.slider.slide.index')
                        ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('slider::slides.title.slides')]));
    }

}
