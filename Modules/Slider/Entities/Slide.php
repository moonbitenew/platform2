<?php

namespace Modules\Slider\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model {

    use Translatable;

    protected $table = 'slider__slides';
    public $translatedAttributes = ['title', 'content', 'url'];
    protected $fillable = [
        'slider_id',
        'file_id'
    ];

}
