<?php

namespace Modules\Slider\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Page\Entities\Page;

class SliderWasCreated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $sliderId;

    /**
     * @var Page
     */
    public $slider;

    public function __construct($sliderId, array $data, $slider) {
        $this->data = $data;
        $this->$sliderId = $sliderId;
        $this->slider = $slider;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->slider;
    }

}
